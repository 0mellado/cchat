#ifndef HTTP_PARSER_H
#define HTTP_PARSER_H

#include <http-server.h>
#include <http/http_request.h>

int parce_http_request(Request **req, char *buffer_req);

#endif