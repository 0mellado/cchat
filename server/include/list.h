#ifndef LIST_H
#define LIST_H

#include <client.h>
#include <stdlib.h>

struct Node_client {
    struct Node_client *next;
    struct Node_client *prev;
    Client *client;
};

typedef struct {
    size_t count;
    struct Node_client *head;
    struct Node_client *tail;
} List_clients;

void init_list(List_clients *list);
void add(List_clients *list, Client *client);
void del(List_clients *list, Client *client);

#endif