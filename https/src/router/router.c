#include <router/router.h>
#include <stdio.h>
#include <stdlib.h>

Router *create_router() {
    Router *router = (Router *)malloc(sizeof(Router));

    router->maps = (Hash_map **)malloc(sizeof(Hash_map *) * MAX_HTTP_METHODS);

    for (size_t i = 0; i < MAX_HTTP_METHODS; i++)
        router->maps[i] = NULL;

    return router;
}

void add_route(Router *router, char *route, enum http_method method, void (*handler)(Request *, Response **)) {
    if (!router->maps[method])
        router->maps[method] = create_hash_map(13); // TODO: implement algorithm to know the best size of hash map

    set_in_hash_map(router->maps[method], route, handler);
}

void add_default_error(Router *router, void (*handler)(Request *, Response **)) {
    router->handler_error = handler;
}

void (*call_function_route(Router *router, const char *route, enum http_method method))(Request *, Response **) {

    if (!router->maps[method])
        return NULL;

    void (*handler)(Request *, Response **) = get_from_hash_map(router->maps[method], route);

    if (!handler)
        return router->handler_error;

    return handler;
}

void destroy_router(Router *router) {
    for (size_t i = 0; i < MAX_HTTP_METHODS; i++)
        if (router->maps[i] != NULL)
            destroy_hash_map(router->maps[i]);

    free(router->maps);
    free(router);
}
