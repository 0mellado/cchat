#include <process_path.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

void str_clean(char **dest, char *src, const char *pattern) {
    size_t pattern_len = strlen(pattern);
    size_t src_len = strlen(src);

    size_t n_patterns = 0;

    for (size_t i = 0; i < src_len - pattern_len; i++)
        if (memcmp(&(src[i]), pattern, pattern_len) == 0)
            n_patterns++;

    size_t total_len = src_len - (n_patterns * pattern_len);
    (*dest) = (char *)malloc(total_len);

    size_t j = 0;
    for (size_t i = 0; i < src_len;) {
        if ((memcmp(&(src[i]), pattern, pattern_len) == 0) && (i < (src_len - pattern_len))) {
            i += pattern_len;
            continue;
        }
        (*dest)[j] = src[i];
        j++;
        i++;
    }
}

char *process_path(char *path) {
    size_t path_len = strlen(path);
    uint8_t find_dir_back = 0;
    for (size_t i = 0; i < path_len - 3; i++)
        if (memcmp(&(path[i]), "/..", 3) == 0)
            find_dir_back = 1;

    if (!find_dir_back)
        return path;

    char *tmp_path = (char *)malloc(path_len + 1);
    memset(tmp_path, 0, path_len + 1);
    strncpy(tmp_path, path, path_len);
    free(path);

    str_clean(&path, tmp_path, "/..");

    free(tmp_path);

    return process_path(path);
}
