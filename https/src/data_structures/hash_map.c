#include <data_structures/hash_map.h>
#include <stdlib.h>
#include <string.h>

Hash_map *create_hash_map(size_t _size) {
    Hash_map *map = (Hash_map *)malloc(sizeof(Hash_map));

    map->count = 0;
    map->size = _size;
    map->items = allocate_lists(_size);

    return map;
}

static List **allocate_lists(size_t size) {
    List **items = (List **)malloc(sizeof(List *) * size);

    for (size_t i = 0; i < size; i++)
        items[i] = NULL;

    return items;
}

size_t hash_func(Hash_map *map, const char *key) {
    size_t sum = 0;
    size_t len = strlen(key);
    for (size_t i = 0; key[i]; i++)
        sum += (size_t)(key[i] ^ len) << (i + 1);

    return sum % map->size;
}

void set_in_hash_map(Hash_map *map, char *key, void (*handler)(Request *, Response **)) {
    size_t index = hash_func(map, key);

    map->alpha = (double)map->count / (double)map->size;

    if (map->items[index] == NULL)
        map->items[index] = create_list();

    struct Hash_map_node *new_map = (struct Hash_map_node *)malloc(sizeof(struct Hash_map_node));

    new_map->handler = handler;
    new_map->key = key;
    map->count++;

    add_to_list(map->items[index], new_map);
}

void (*get_from_hash_map(Hash_map *map, const char *key))(Request *, Response **) {
    size_t index = hash_func(map, key);

    if (!map->items[index])
        return NULL;

    List *list = map->items[index];

    struct List_node *list_node = list->head;

    struct Hash_map_node *map_node = (struct Hash_map_node *)list_node->data;

    uint8_t not_find = 0;
    while (memcmp(map_node->key, key, strlen(key)) != 0) {
        list_node = list_node->next;
        if (!list_node) {
            not_find = 1;
            break;
        }
        map_node = (struct Hash_map_node *)list_node->data;
    }

    if (not_find)
        return NULL;

    return map_node->handler;
}

void destroy_hash_map(Hash_map *map) {
    for (size_t i = 0; i < map->size; i++)
        if (map->items[i] != NULL)
            destroy_list(map->items[i]);

    free(map->items);
    free(map);
}