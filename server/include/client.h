#ifndef CLIENT_H
#define CLIENT_H
#include <config.h>
#include <netinet/in.h>

enum client_status {
    TO_WRITE,
    TO_READ,
    TO_CLOSE
};

typedef struct {
    int fd;
    uint8_t admin;
    char nick[MAX_LEN_NICK];
    struct sockaddr_in addr;
    char msg[MAX_LEN_MSG];
    enum client_status status;
    uint8_t force_status;
} Client;

#endif