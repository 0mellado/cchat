#include <data_structures/queue.h>
#include <stdlib.h>

static node_t *head = NULL;
static node_t *tail = NULL;

void enqueue(struct Connection *client) {
    node_t *new_node = (node_t *)malloc(sizeof(node_t));
    new_node->client = client;
    new_node->next = NULL;

    if (tail == NULL)
        head = new_node;
    else
        tail->next = new_node;

    tail = new_node;
}

struct Connection *dequeue() {
    if (head == NULL)
        return NULL;
    struct Connection *client = head->client;
    node_t *temp = head;
    head = head->next;
    if (head == NULL)
        tail = NULL;
    free(temp);
    return client;
}
