#include <config.h>
#include <core/file_io.h>
#include <errno.h>
#include <fcntl.h>
#include <process_path.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <unistd.h>

int read_file(char *file_path, char **buffer, size_t *size) {
    struct stat file_stats;
    char path[256] = {0};
    file_path = process_path(file_path);
    snprintf(path, 256, "%s%s", PUBLIC_PATH, file_path);
    printf("[*] Leyendo el archivo %s\n", path);

    int fd = open(path, O_RDONLY);
    if (fd < 0) {
        perror("Error");
        return -1;
    }

    if (fstat(fd, &file_stats) < 0) {
        perror("Error");
        close(fd);
        return -1;
    }

    printf("[*] Tamaño del archivo: %ld\n", file_stats.st_size);
    (*size) = file_stats.st_size;
    (*buffer) = (char *)malloc(file_stats.st_size);
    if (!(*buffer)) {
        perror("Error");
        close(fd);
        return -1;
    }

    memset((*buffer), 0, file_stats.st_size);

    ssize_t bytes_read = read(fd, (*buffer), file_stats.st_size);
    if (bytes_read < 0 || (size_t)bytes_read < file_stats.st_size) {
        perror("Error");
        free((*buffer));
        close(fd);
        return -1;
    }

    close(fd);
    return 1;
}