#ifndef QUEUE_H
#define QUEUE_H
#include <http-server.h>

typedef struct Node {
    struct Node *next;
    struct Connection *client;
} node_t;

void enqueue(struct Connection *client);

struct Connection *dequeue();

#endif