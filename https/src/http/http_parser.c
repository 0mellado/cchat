#include <http/http_methods.h>
#include <http/http_parser.h>

static size_t get_method(char *buffer, enum http_method *method) {
    if (memcmp(buffer, "GET", 3) == 0) {
        *method = GET_METHOD;
        return 4;
    } else if (memcmp(buffer, "PUT", 3) == 0) {
        *method = PUT_METHOD;
        return 4;
    } else if (memcmp(buffer, "HEAD", 4) == 0) {
        *method = HEAD_METHOD;
        return 5;
    } else if (memcmp(buffer, "POST", 4) == 0) {
        *method = POST_METHOD;
        return 5;
    } else if (memcmp(buffer, "PATCH", 5) == 0) {
        *method = PATCH_METHOD;
        return 6;
    } else if (memcmp(buffer, "TRACE", 5) == 0) {
        *method = TRACE_METHOD;
        return 6;
    } else if (memcmp(buffer, "DELETE", 6) == 0) {
        *method = DELETE_METHOD;
        return 7;
    } else if (memcmp(buffer, "CONNECT", 7) == 0) {
        *method = CONNECT_METHOD;
        return 8;
    } else if (memcmp(buffer, "OPTIONS", 7) == 0) {
        *method = OPTIONS_METHOD;
        return 8;
    }
}

int parce_http_request(Request **req, char *buffer_req) {
    (*req) = (Request *)malloc(sizeof(Request));

    if (!(*req))
        return -1;

    size_t last_pos = get_method(buffer_req, &((*req)->method));
    size_t i = last_pos;
    char c = buffer_req[last_pos];
    while (c > ' ') {
        buffer_req[i - last_pos] = c;
        i++;
        c = buffer_req[i];
    }
    i++;
    (*req)->path = (char *)malloc(i - last_pos);
    memcpy((*req)->path, buffer_req, (i - last_pos) - 1);
    (*req)->path[(i - last_pos) - 1] = 0;

    return 0;
}