#ifndef UTILS_H
#define UTILS_H

char *clear(const char *str, size_t len);

char **split_cmd(const char *cmd, int *argc);

uint8_t str_cmp(const char *s1, const char *s2);

#endif