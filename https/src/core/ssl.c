#include <config.h>
#include <core/ssl.h>

SSL *init_ssl_connection(int client_fd) {
    int result;
    SSL_CTX *ctx = SSL_CTX_new(TLS_server_method());
    SSL *ssl = SSL_new(ctx);
    SSL_set_fd(ssl, client_fd);
    result = SSL_use_certificate_chain_file(ssl, CERTIFICATE_PATH);
    if (result != 1) {
        SSL_shutdown(ssl);
        return NULL;
    }
    result = SSL_use_PrivateKey_file(ssl, KEY_PATH, SSL_FILETYPE_PEM);
    if (result != 1) {
        SSL_shutdown(ssl);
        return NULL;
    }
    result = SSL_accept(ssl);
    result = SSL_get_error(ssl, result);

    result = check_ssl(result);

    if (result == -1) {
        SSL_shutdown(ssl);
        return NULL;
    }

    return ssl;
}

int check_ssl(int return_value) {
    switch (return_value) {
    case SSL_ERROR_NONE:
        printf("[*] SSL funcionó correctamente\n");
        return 0;
    case SSL_ERROR_ZERO_RETURN:
        printf("[*] SSL_ERROR_ZERO_RETURN\n");
        return -1;
    case SSL_ERROR_WANT_READ:
        printf("[*] SSL_ERROR_WANT_READ\n");
        return -1;
    case SSL_ERROR_WANT_WRITE:
        printf("[*] SSL_ERROR_WANT_WRITE\n");
        return -1;
    case SSL_ERROR_WANT_CONNECT:
        printf("[*] SSL_ERROR_WANT_CONNECT\n");
        return -1;
    case SSL_ERROR_WANT_ACCEPT:
        printf("[*] SSL_ERROR_WANT_ACCEPT\n");
        return -1;
    case SSL_ERROR_WANT_X509_LOOKUP:
        printf("[*] SSL_ERROR_WANT_X509_LOOKUP\n");
        return -1;
    case SSL_ERROR_SYSCALL:
        printf("[*] SSL_ERROR_SYSCALL\n");
        return -1;
    case SSL_ERROR_SSL:
        printf("[*] SSL_ERROR_SSL\n");
        return -1;
    default:
        return 0;
    }
}

int ssl_write(SSL *ssl, const void *buffer, size_t n_bytes) {
    return SSL_get_error(ssl, SSL_write(ssl, buffer, n_bytes));
}

int ssl_read(SSL *ssl, void *buffer, size_t n_bytes) {
    return SSL_get_error(ssl, SSL_read(ssl, buffer, n_bytes));
}