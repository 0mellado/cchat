#ifndef ROUTER_H
#define ROUTER_H
#include <data_structures/hash_map.h>
#include <http/http_methods.h>
#include <http/http_request.h>
#include <http/http_response.h>

typedef struct _Router {
    Hash_map **maps;
    void (*handler_error)(Request *, Response **);
} Router;

Router *create_router();

void add_route(Router *router, char *route, enum http_method method, void (*handler)(Request *, Response **));

void add_default_error(Router *router, void (*handler)(Request *, Response **));

void (*call_function_route(Router *router, const char *route, enum http_method method))(Request *, Response **);

void destroy_router(Router *router);

#endif