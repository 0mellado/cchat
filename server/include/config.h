#ifndef CONFIG_H
#define CONFIG_H

#define PROMPT "> "
#define MAX_CLIENTS 100
#define MAX_LEN_MSG 985
#define MAX_LEN_NICK 32
#define MAX_LEN 1024
#define HOST "0.0.0.0"
#define PORT (uint16_t)8080

#endif