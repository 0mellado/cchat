#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <utils.h>

char *clear(const char *str, size_t len) {
    size_t n_spaces;
    size_t begin = 0;
    size_t end = len - 1;
    char c = str[begin];
    while (c < 33) {
        begin++;
        c = str[begin];
    }
    n_spaces = begin;

    c = str[end];
    while (c < 33) {
        end--;
        c = str[end];
    }
    n_spaces += len - 1 - end;

    char *new_str = (char *)malloc(sizeof(char) * len - n_spaces + 1);

    for (size_t i = begin; i < end + 1; i++)
        new_str[i - begin] = str[i];

    new_str[len - n_spaces] = 0;

    return new_str;
}

char **split_cmd(const char *cmd, int *argc) {
    size_t size = strlen(cmd);
    char *str_clean = clear(cmd, size);
    size = strlen(str_clean);
    char buffer_cmd[32];
    size_t n_split = 0;
    uint8_t i = 0;
    size_t last_len = 0;
    *argc = 0;
    char c;

    for (size_t x = 0; x < size; x++)
        if ((str_clean[x] < 33) && (str_clean[x + 1] > 33))
            n_split++;

    char **argv = (char **)malloc(sizeof(char *) * (n_split + 1));

    while (((*argc) - 1) != n_split) {
        memset(buffer_cmd, 0, 32);
        c = str_clean[last_len];

        while (c < 33)
            c = str_clean[++last_len];

        while (c >= 33) {
            buffer_cmd[i] = c;
            i++;
            c = str_clean[i + last_len];
        }
        last_len += i + 1;

        argv[(*argc)] = (char *)malloc(i + 2);
        memcpy(argv[(*argc)], buffer_cmd, i + 1);
        argv[(*argc)][i + 1] = 0;

        i = 0;
        (*argc)++;
    }
    free(str_clean);

    return argv;
}

uint8_t str_cmp(const char *s1, const char *s2) {
    size_t len_s1 = strlen(s1);
    size_t len_s2 = strlen(s2);

    if (len_s1 != len_s2)
        return 0;

    for (size_t i = 0; i < len_s1; i++) {
        if (s1[i] != s2[i])
            return 0;
    }

    return 1;
}
