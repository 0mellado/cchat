#include <server.h>
#include <stdio.h>

int main(int argc, char **argv) {

    struct Server *server = init_server(8080, "Iniciando servidor ...");

    printf("ip: %s, port: %d\n", server->address, server->port);

    run(server);

    stop_server(server);

    return 0;
}
