#ifndef HTTP_REQUEST_H
#define HTTP_REQUEST_H
#include <http/http_methods.h>

typedef struct _Request {
    enum http_method method;
    char *path;
} Request;

#endif