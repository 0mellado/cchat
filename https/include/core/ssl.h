#ifndef SSL_H
#define SSL_H

#include <openssl/ssl.h>

SSL *init_ssl_connection(int client_fd);

int check_ssl(int return_value);

int ssl_write(SSL *ssl, const void *buffer, size_t n_bytes);
int ssl_read(SSL *ssl, void *buffer, size_t n_bytes);

#endif