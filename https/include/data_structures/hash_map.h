#ifndef HASH_MAP_H
#define HASH_MAP_H
#include <data_structures/list.h>
#include <http/http_request.h>
#include <http/http_response.h>

struct Hash_map_node {
    void (*handler)(Request *, Response **);
    char *key;
};

typedef struct _Hash_map {
    List **items;
    size_t count;
    size_t size;
    double alpha;
} Hash_map;

static List **allocate_lists(size_t size);
// static void realloc_hash_map(Hash_map *map); /* TODO implement realloc functions when alpha > 0.75 */

Hash_map *create_hash_map(size_t size);
void set_in_hash_map(Hash_map *map, char *key, void (*handler)(Request *, Response **));
void (*get_from_hash_map(Hash_map *map, const char *key))(Request *, Response **);

size_t hash_func(Hash_map *map, const char *key);

void destroy_hash_map(Hash_map *map);

#endif