#ifndef PROCESS_PATH_H
#define PROCESS_PATH_H

static void str_clean(char **dest, char *src, const char *pattern);

char *process_path(char *path);

#endif