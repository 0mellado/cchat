#include <arpa/inet.h>
#include <config.h>
#include <list.h>
#include <sys/types.h>

struct Server {
    int fd;
    int epoll_fd;
    char address[INET_ADDRSTRLEN];
    u_int16_t port;
    List_clients *clients;
};

struct Server *init_server(u_int16_t port, const char *caption);

void run(struct Server *server);

int stop_server(struct Server *server);