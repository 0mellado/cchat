#ifndef HTTP_RESPONSE_H
#define HTTP_RESPONSE_H

#include <stddef.h>
#include <stdint.h>

typedef struct _Response {
    char *header;
    size_t content_len;
    char *content;
    uint16_t status_code;
} Response;

#endif