#ifndef THREAD_POOL_H
#define THREAD_POOL_H

#include <config.h>
#include <pthread.h>
#include <stdint.h>

typedef struct _Thread_Pool {
    size_t n_threads;
    volatile uint8_t active;
    pthread_mutex_t mutex;
    pthread_cond_t condition_var;
    pthread_t *pool;
} Thread_pool;

void init_thread_pool(Thread_pool *thp, size_t _n_threads);
void *thread_job(void *args);
void destroy_thread_pool(Thread_pool *thp);

#endif