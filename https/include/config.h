#ifndef CONFIG_H
#define CONFIG_H

#define BACKLOG 100

#define THREAD_POOL_SIZE 4

#define PUBLIC_PATH "/home/oscar/proyectos/chat/https/public"

#define CERTIFICATE_PATH "/home/oscar/proyectos/chat/https/127.0.0.1/cert.pem"
#define KEY_PATH "/home/oscar/proyectos/chat/https/127.0.0.1/key.pem"

#endif