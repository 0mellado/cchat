#ifndef LIST_H
#define LIST_H
#include <stddef.h>

struct List_node {
    struct List_node *next;
    void *data;
};

typedef struct _List {
    struct List_node *head, *tail;
    size_t size;
} List;

List *create_list();

void add_to_list(List *list, void *val);

void destroy_list(List *list);

#endif