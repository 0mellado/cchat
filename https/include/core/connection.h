#ifndef CONNECTION_H
#define CONNECTION_H
#include <http/http_request.h>
#include <http/http_response.h>
#include <openssl/ssl.h>

struct Connection {
    SSL *ssl;
    int fd;
    Request *req;
    Response *res;
    void (*route_handler)(Request *, Response **);
};

#endif