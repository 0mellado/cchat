#include <data_structures/list.h>
#include <stdlib.h>

List *create_list() {
    List *list = (List *)malloc(sizeof(List));

    list->size = 0;
    list->head = NULL;
    list->tail = NULL;

    return list;
}

void add_to_list(List *list, void *val) {
    if (!list)
        return;

    struct List_node *new_node = (struct List_node *)malloc(sizeof(struct List_node));
    new_node->data = val;
    new_node->next = NULL;

    if (list->size == 0) {
        list->head = new_node;
        list->tail = new_node;
        list->size++;
        return;
    }

    list->tail->next = new_node;
    list->tail = new_node;
    list->size++;
}

void destroy_list(List *list) {
    if (list->size != 0) {
        struct List_node *node = list->head;

        struct List_node *tmp = NULL;
        while (node != NULL) {
            free(node->data);
            tmp = node;
            node = node->next;
            free(tmp);
        }
    }

    free(list);
}