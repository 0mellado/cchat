#ifndef SERVER_H
#define SERVER_H

#include <core/thread_pool.h>
#include <netinet/in.h>
#include <router/router.h>

typedef struct _ServerHTTP {
    int fd;
    int backlog;
    char address[INET_ADDRSTRLEN];
    volatile uint8_t running;
    uint16_t port;
    Thread_pool thread_pool;
    Router *router_maps;
} ServerHTTP;

#endif