#include <client.h>
#include <ncurses.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/epoll.h>
#include <sys/socket.h>
#include <unistd.h>

void check(int val, const char *msg) {
    if (val < 0) {
        fprintf(stderr, "%s\n", msg);
        exit(EXIT_FAILURE);
    }
}

void epoll_init(struct Server *server) {
    check(server->epoll_fd = epoll_create1(0), "Error epoll_create1");

    struct epoll_event ev;
    ev.data.fd = server->fd;
    ev.events = EPOLLIN | EPOLLHUP | EPOLLRDHUP;
    check(epoll_ctl(server->epoll_fd, EPOLL_CTL_ADD, server->fd, &ev), "Error epoll_ctl 0");

    ev.data.fd = STDIN_FILENO;
    ev.events = EPOLLIN;
    check(epoll_ctl(server->epoll_fd, EPOLL_CTL_ADD, STDIN_FILENO, &ev), "Error epoll_ctl 1");
}

void run(struct Server *server) {
    uint8_t running = 1;
    struct epoll_event events[100];
    int ready_fds;
    char buffer[256];
    char *msg = NULL;
    struct epoll_event ev;

    while (running) {
        check(ready_fds = epoll_wait(server->epoll_fd, events, 100 - 1, -1), "Error epoll_wait");

        for (size_t i = 0; i < ready_fds; i++) {
            if (events[i].data.fd == server->fd) {
                if ((events[i].events & EPOLLRDHUP) || (events[i].events & EPOLLHUP)) {
                    printf("Servidor desconectado\nCerrando...\n");
                    running = 0;
                    break;
                } else if (events[i].events & EPOLLIN) {
                    int result;
                    memset(buffer, 0, 256);
                    check(result = recv(server->fd, buffer, 256, 0), "Error recv");

                    if (result == 0) {
                        printf("Mensaje vacio\nCerrando...\n");
                        running = 0;
                        break;
                    }

                    write(STDOUT_FILENO, buffer, 256);
                } else if (events[i].events & EPOLLOUT) {
                    check(send(server->fd, msg, 256, 0), "Error send");
                    ev.data.fd = server->fd;
                    ev.events = EPOLLIN | EPOLLHUP | EPOLLRDHUP;
                    check(epoll_ctl(server->epoll_fd, EPOLL_CTL_MOD, server->fd, &ev), "Error epoll_ctl 2");
                }
            } else {
                ssize_t n_reads;
                size_t len;
                check(n_reads = getline(&msg, &len, stdin), "Error getline");
                ev.data.fd = server->fd;
                ev.events = EPOLLOUT | EPOLLRDHUP | EPOLLHUP;
                check(epoll_ctl(server->epoll_fd, EPOLL_CTL_MOD, server->fd, &ev), "Error epoll_ctl 3");
            }
        }
    }
}

struct Server *connect_server(const char *addr, u_int16_t port) {
    struct Server *server = (struct Server *)malloc(sizeof(struct Server));

    strcpy(server->address, addr);
    server->port = port;
    check(server->fd = socket(AF_INET, SOCK_STREAM, 0), "Error socket");

    epoll_init(server);

    struct sockaddr_in server_addr;
    server_addr.sin_family = AF_INET;
    server_addr.sin_port = htons(port);
    inet_pton(AF_INET, addr, &(server_addr.sin_addr));

    check(connect(server->fd, (struct sockaddr *)&server_addr, sizeof(server_addr)), "Error connect");

    return server;
}

void close_server(struct Server *server) {
    check(close(server->fd), "Error close");
}