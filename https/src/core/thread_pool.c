#include <core/thread_pool.h>
#include <data_structures/queue.h>
#include <http-server.h>

void init_thread_pool(Thread_pool *thp, size_t _n_threads) {

    thp->active = 1;
    thp->n_threads = _n_threads;
    thp->pool = (pthread_t *)malloc(sizeof(pthread_t) * thp->n_threads);

    pthread_mutex_init(&(thp->mutex), NULL);
    pthread_cond_init(&(thp->condition_var), NULL);

    for (size_t i = 0; i < thp->n_threads; i++)
        pthread_create(&(thp->pool[i]), NULL, thread_job, thp);
}

void *thread_job(void *arg) {
    Thread_pool *thp = (Thread_pool *)arg;
    while (thp->active) {
        struct Connection *client;
        pthread_mutex_lock(&(thp->mutex));

        if ((client = dequeue()) == NULL) {
            pthread_cond_wait(&(thp->condition_var), &(thp->mutex));
            client = dequeue();
        }

        pthread_mutex_unlock(&(thp->mutex));
        if (client != NULL)
            handle_connection(client);
    }
}

void destroy_thread_pool(Thread_pool *thp) {
    printf("[*] Apagando thread pool ...\n");
    thp->active = 0;
    for (size_t i = 0; i < thp->n_threads; i++)
        pthread_cond_signal(&(thp->condition_var));

    for (size_t i = 0; i < thp->n_threads; i++)
        pthread_join(thp->pool[i], NULL);

    pthread_mutex_destroy(&(thp->mutex));
    pthread_cond_destroy(&(thp->condition_var));
    free(thp->pool);
}