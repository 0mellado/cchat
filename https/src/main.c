#include <http-server.h>
#include <stdio.h>

void handler_root_get(Request *, Response **res) {
    char *hola = "<h1>hola como estai</h1>\r\n<h2>esto es una prueba</h2>";
    (*res)->content_len = strlen(hola);
    (*res)->status_code = 200;
    (*res)->content = (char *)malloc(strlen(hola) + 1);
    memcpy((*res)->content, hola, strlen(hola) + 1);
}

void handler_error(Request *, Response **res) {
    (*res)->status_code = 404;
    char *error404 = "<h1>Error 404 not found</h1>";
    (*res)->content_len = strlen(error404);
    (*res)->content = (char *)malloc((*res)->content_len + 1);
    memcpy((*res)->content, error404, (*res)->content_len + 1);
}

int main(int argc, char **argv) {

    Router *router = create_router();

    init_server("0.0.0.0", 8000, 100);

    add_route(router, "/", GET_METHOD, handler_root_get);

    add_default_error(router, handler_error);

    set_router(router);

    run_server();

    close_server();

    return 0;
}
