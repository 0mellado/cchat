#ifndef HTTP_SERVER_H
#define HTTP_SERVER_H

#include <config.h>
#include <core/connection.h>
#include <router/router.h>

void init_server(const char *address, uint16_t port, int backlog);

void set_router(Router *router);

void run_server();

void handle_connection(struct Connection *client);

void close_server();

#endif