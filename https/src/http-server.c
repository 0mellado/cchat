#include <arpa/inet.h>
#include <core/file_io.h>
#include <core/server.h>
#include <core/ssl.h>
#include <data_structures/queue.h>
#include <errno.h>
#include <fcntl.h>
#include <http-server.h>
#include <http/http_parser.h>
#include <process_path.h>
#include <signal.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

static ServerHTTP *server = NULL;
static char *header =
    "HTTP/1.1 %d OK\r\n"
    "Server: Apache/2.2.3\r\n"
    "Content-Type: text/html\r\n"
    "Content-Length: %lu\r\n"
    "Accept-Ranges: bytes\r\n"
    "Connection: keep-alive\r\n"
    "\r\n"
    "%s";

#define CHECK(val, msg)         \
    {                           \
        if (val < 0) {          \
            perror(msg);        \
            exit(EXIT_FAILURE); \
        }                       \
    }

void send_response(struct Connection *client) {
    client->res->header = header;
    size_t header_len = strlen(header);
    size_t response_len = header_len + client->res->content_len;

    char *response = (char *)malloc(response_len);
    if (!response) {
        perror("Error");
        return;
    }

    snprintf(response, response_len, header, client->res->status_code, client->res->content_len, client->res->content);
    printf("[*] Enviando respuesta del cliente %d\n", client->fd);
    pthread_mutex_lock(&(server->thread_pool.mutex));
    check_ssl(ssl_write(client->ssl, response, response_len));
    pthread_mutex_unlock(&(server->thread_pool.mutex));
    free(response);
}

int read_request(struct Connection *client, char *buffer) {
    pthread_mutex_lock(&(server->thread_pool.mutex));
    int result = check_ssl(ssl_read(client->ssl, buffer, (size_t)1023));
    pthread_mutex_unlock(&(server->thread_pool.mutex));
    if (result == -1)
        return result;
    return 0;
}

void clean_client(struct Connection *client, uint8_t zero_bytes) {
    printf("Limpiando cliente %d\n", client->fd);
    close(client->fd);

    if (!zero_bytes) {
        free(client->req->path);
        free(client->res->content);
    }

    free(client->req);
    free(client->res);
    SSL_shutdown(client->ssl);
    free(client);
}

void init_server(const char *address, uint16_t port, int backlog) {
    server = (ServerHTTP *)malloc(sizeof(ServerHTTP));
    server->backlog = backlog;
    server->running = 1;
    struct sockaddr_in addr;
    int result;
    int opt = 1;
    server->fd = socket(AF_INET, SOCK_STREAM, 0);
    CHECK(server->fd, "[SERVER] Socket");
    result = setsockopt(server->fd, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt));
    CHECK(result, "[SERVER] Setsockopt");

    addr.sin_family = AF_INET;
    addr.sin_port = htons(port);
    inet_pton(AF_INET, address, &(addr.sin_addr));

    result = bind(server->fd, (struct sockaddr *)&addr, sizeof(addr));
    CHECK(result, "[SERVER] Bind");

    result = listen(server->fd, server->backlog);
    CHECK(result, "[SERVER] Listen");

    init_thread_pool(&server->thread_pool, (size_t)THREAD_POOL_SIZE);
}

struct Connection *accept_connection() {
    struct Connection *client = (struct Connection *)malloc(sizeof(struct Connection));

    client->fd = accept(server->fd, NULL, NULL);
    CHECK(client->fd, "[SERVER] Accept");
    printf("[*] Se ha conectado un cliente con el fd %d\n", client->fd);

    client->ssl = init_ssl_connection(client->fd);
    if (client->ssl == NULL) {
        close(client->fd);
        free(client);
        return NULL;
    }
    printf("[*] Cliente conectado con ssl\n");

    client->res = (Response *)malloc(sizeof(Response));

    return client;
}

void handle_connection(struct Connection *client) {
    printf("[*] Procesando petición ...\n");
    char buffer[1024] = {0};
    int result;

    result = read_request(client, buffer);

    if (result == -1) {
        clean_client(client, 1);
        return;
    }

    result = parce_http_request(&(client->req), buffer);

    if (result == -1) {
        clean_client(client, 1);
        return;
    }

    printf("[*] Metodo %d en la ruta %s\n", client->req->method, client->req->path);

    // aca va el middleware

    // aca va el router

    client->route_handler = call_function_route(server->router_maps, client->req->path, client->req->method);

    if (!client->route_handler) {
        clean_client(client, 0);
        return;
    }

    client->route_handler(client->req, &client->res);

    printf("[*] Enviando respuesta al cliente %d\n", client->fd);
    send_response(client);

    clean_client(client, 0);
    return;
}

static void quit_handler(int sig) {
    printf("\r[*] Apagando servidor web ...\n");
    signal(sig, SIG_IGN);
    close_server();
}

void run_server() {
    printf("Escuchando conexiones ...\n");

    signal(SIGINT, quit_handler);

    while (server->running) {
        struct Connection *client = accept_connection();
        if (client == NULL)
            continue;

        pthread_mutex_lock(&(server->thread_pool.mutex));
        enqueue(client);
        pthread_cond_signal(&(server->thread_pool.condition_var));
        pthread_mutex_unlock(&(server->thread_pool.mutex));
    }
    printf("[*] Hola el servidor se esta cerrando ...\n");
}

void set_router(Router *router) {
    server->router_maps = router;
}

void close_server() {
    destroy_thread_pool(&(server->thread_pool));
    destroy_router(server->router_maps);
    close(server->fd);
    free(server);
}