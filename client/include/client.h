#ifndef CLIENT_H
#define CLIENT_H
#include <arpa/inet.h>
#include <sys/types.h>

struct Server {
    int fd;
    char address[INET_ADDRSTRLEN];
    int epoll_fd;
    u_int16_t port;
};

struct Server *connect_server(const char *addr, u_int16_t port);

void run(struct Server *server);

void close_server(struct Server *server);

#endif