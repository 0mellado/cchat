#include <list.h>
#include <stdio.h>

void init_list(List_clients *list) {
    list->count = 0;
    list->head = NULL;
    list->tail = NULL;
}

void add(List_clients *list, Client *client) {
    struct Node_client *new_node = (struct Node_client *)malloc(sizeof(struct Node_client));
    new_node->client = client;
    new_node->next = NULL;
    new_node->prev = NULL;

    if (list->count == 0) {

        list->head = new_node;
        list->tail = new_node;
        list->count++;
        printf("[SERVER::CLIENT_FDS] Se añadio un cliente con el fd: %d\n", client->fd);
        return;
    }

    list->tail->next = new_node;
    new_node->prev = list->tail;
    list->tail = new_node;

    list->count++;

    printf("[SERVER::CLIENT_FDS] Se añadio un cliente con el fd: %d\n", client->fd);
    return;
}

void del(List_clients *list, Client *client) {
    if (list->count == 0)
        return;

    struct Node_client *del_node;

    if (list->head->client->fd == client->fd) {
        printf("[SERVER::CLIENT_FDS] Se eliminó un cliente con el fd: %d\n", client->fd);
        del_node = list->head;
        if (list->count != 1)
            list->head = del_node->next;

        free(del_node->client);
        free(del_node);

        list->count--;

        return;
    }

    if (list->tail->client->fd == client->fd) {
        printf("[SERVER::CLIENT_FDS] Se eliminó un cliente con el fd: %d\n", client->fd);
        del_node = list->tail;
        list->tail = del_node->prev;

        free(del_node->client);
        free(del_node);

        list->count--;

        return;
    }

    printf("[SERVER::CLIENT_FDS] Se eliminó un cliente con el fd: %d\n", client->fd);

    del_node = list->head;

    while (del_node->client->fd != client->fd)
        del_node = del_node->next;

    del_node->prev->next = del_node->next;
    del_node->next->prev = del_node->prev;

    free(del_node->client);
    free(del_node);

    list->count--;
}