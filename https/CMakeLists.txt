cmake_minimum_required(VERSION 3.27.0)
project(httpserver VERSION 0.1.0 LANGUAGES C)

include(CTest)
find_package(OpenSSL REQUIRED)
set(CMAKE_C_COMPILER "gcc")
include_directories(
        ${PROJECT_SOURCE_DIR}/include
)

enable_testing()


file(GLOB_RECURSE all_SRCS
    "${PROJECT_SOURCE_DIR}/src/*.cpp"
    "${PROJECT_SOURCE_DIR}/src/*.c"
)

add_executable(httpserver ${all_SRCS})
target_link_libraries(httpserver PRIVATE OpenSSL::SSL)

set(CPACK_PROJECT_NAME ${PROJECT_NAME})
set(CPACK_PROJECT_VERSION ${PROJECT_VERSION})
include(CPack)
