#include <commands.h>
#include <stdio.h>
#include <string.h>

void setnick_handler(Client *client, int argc, char **argv) {
    printf("Comando /setnick, cliente %d\n", client->fd);
    if (argc != 2) {
        printf("[SERVER] Número incorrecto de argumentos, cliente %d\n", client->fd);
        strncpy(client->msg, "[Server] Número incorrecto de argumentos\nUso: /setnick <nick>\n", 256);
    } else {
        memset(client->nick, 0, 32);
        strncpy(client->nick, argv[1], 32);

        snprintf(client->msg, 256, "[Server] Nombre cambiado a %s\n", client->nick);
        printf("[SERVER] Nombre cambiado a %s, cliente %d\n", client->nick, client->fd);
    }
}