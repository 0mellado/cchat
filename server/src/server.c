#include <commands.h>
#include <fcntl.h>
#include <netinet/in.h>
#include <server.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/epoll.h>
#include <sys/socket.h>
#include <unistd.h>
#include <utils.h>

void check(int val, const char *msg) {
    if (val < 0) {
        fprintf(stderr, "%s\n", msg);
        exit(EXIT_FAILURE);
    }
}

void init_epoll(struct Server *server) {
    check(server->epoll_fd = epoll_create1(0), "[SERVER] Error epoll_create1");

    struct epoll_event ev;
    ev.data.fd = server->fd;
    ev.events = EPOLLIN;
    check(epoll_ctl(server->epoll_fd, EPOLL_CTL_ADD, server->fd, &ev), "[SERVER] Error epoll_ctl 0");
}

struct Server *init_server(u_int16_t port, const char *caption) {

    int opt = 1;
    struct Server *server = (struct Server *)malloc(sizeof(struct Server));

    check(server->fd = socket(AF_INET, SOCK_STREAM, 0), "[SERVER] Error socket");
    check(setsockopt(server->fd, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt)), "[SERVER] setsockpot error");

    struct sockaddr_in addr;
    addr.sin_family = AF_INET,
    inet_pton(AF_INET, HOST, &(addr.sin_addr));
    addr.sin_port = htons(PORT);
    strcpy(server->address, HOST);
    server->port = PORT;

    check(bind(server->fd, (struct sockaddr *)&addr, sizeof(addr)), "[SERVER] Error bind");
    check(listen(server->fd, MAX_CLIENTS), "[SERVER] Error listen");

    init_epoll(server);
    server->clients = (List_clients *)malloc(sizeof(List_clients));
    init_list(server->clients);

    printf("%s\n", caption);

    return server;
}

void broadcast(struct Server *server, Client *client_tx) {

    struct Node_client *node = server->clients->head;
    struct epoll_event ev;

    for (; node != NULL; node = node->next) {
        if (node->client->fd == client_tx->fd)
            continue;
        snprintf(node->client->msg, MAX_LEN_MSG, "[%.*s] %.*s", MAX_LEN_NICK, client_tx->nick, MAX_LEN_MSG - 40, client_tx->msg);
        node->client->force_status = 1;
        node->client->status = TO_WRITE;

        ev.data.ptr = node->client;
        ev.events = EPOLLOUT | EPOLLRDHUP | EPOLLHUP;
        check(epoll_ctl(server->epoll_fd, EPOLL_CTL_MOD, node->client->fd, &ev), "Error epoll_ctl mod");
    }

    client_tx->force_status = 1;
    client_tx->status = TO_READ;
}

void accept_new_client(struct Server *server) {
    char address[INET_ADDRSTRLEN];
    Client *client = (Client *)malloc(sizeof(Client));
    socklen_t client_addr_size = sizeof(client->addr);

    check(client->fd = accept(server->fd, (struct sockaddr *)&client->addr, &client_addr_size), "[SERVER] Error accept");
    inet_ntop(AF_INET, &(client->addr.sin_addr), address, INET_ADDRSTRLEN);

    client->status = TO_READ;
    client->admin = 0;
    snprintf(client->nick, 32, "client-%d", client->fd);

    struct epoll_event ev;
    ev.data.ptr = client;
    ev.events = EPOLLIN | EPOLLRDHUP | EPOLLHUP;

    add(server->clients, client);

    send(client->fd, PROMPT, 3, 0);
    check(epoll_ctl(server->epoll_fd, EPOLL_CTL_ADD, client->fd, &ev), "[SERVER] Error epoll_ctl add");
    printf("[SERVER] Se ha conectado %s:%u\n", address, ntohs(client->addr.sin_port));
}

void del_client(struct Server *server, Client *client) {
    check(epoll_ctl(server->epoll_fd, EPOLL_CTL_DEL, client->fd, NULL), "[SERVER] Error epoll_ctl del");
    close(client->fd);

    del(server->clients, client);
}

void run_user_command(struct Server *server, Client *client) {

    uint8_t i = 0;
    char c = client->msg[i];
    while (c < 33)
        c = client->msg[++i];

    if (c == '/') {
        printf("[SERVER::CMD] El cliente %d quiere ejecutar un comando\n", client->fd);
        client->status = TO_WRITE;
        int argc;
        char **argv = split_cmd(client->msg, &argc);

        if (str_cmp(argv[0], "/setnick")) {
            setnick_handler(client, argc, argv);
        } else if (str_cmp(argv[0], "/info")) {
            printf("[SERVER] Comando /info, cliente %d\n", client->fd);
            snprintf(client->msg, 256, "[Server] Comando /info\n");
        } else if (str_cmp(argv[0], "/help")) {
            printf("[SERVER] Comando /help, cliente %d\n", client->fd);
            snprintf(client->msg, 256, "[Server] Comando /help\n");
        } else {
            printf("[SERVER] Comando desconocido, cliente %d\n", client->fd);
            snprintf(client->msg, 256, "[Server] Comando desconocido\n");
        }

        for (size_t i = 0; i < argc; i++)
            free(argv[i]);
        free(argv);

        client->force_status = 1;
    } else {
        broadcast(server, client);
    }
}

void serve_client(struct Server *server, Client *client) {
    char msg[256];

    if (client->status == TO_READ) {
        ssize_t size;
        memset(msg, 0, 256);
        size = recv(client->fd, msg, 256, 0);
        send(client->fd, PROMPT, 3, 0);

        if (size <= 0) {
            if (size == 0)
                printf("[SERVER] Mensaje vacio desde el cliente: %d\n", client->fd);
            else
                fprintf(stderr, "[SERVER] Error al recibir mensaje del cliente: %d", client->fd);

            client->status = TO_CLOSE;
        } else {
            printf("[SERVER] El client %d envió un mensaje: %s", client->fd, msg);
            strncpy(client->msg, msg, 256);

            run_user_command(server, client);

            if (client->force_status == 0) {
                printf("[SERVER] El cliente %d, preparado para escribir\n", client->fd);
                client->status = TO_WRITE;
            }
        }
    } else if (client->status == TO_WRITE) {
        char new_msg[MAX_LEN];
        snprintf(new_msg, MAX_LEN, "\r%s%s", client->msg, PROMPT);

        if (send(client->fd, new_msg, strlen(new_msg), 0) == -1) {
            fprintf(stderr, "[SERVER] Error al enviar mensaje al cliente: %d", client->fd);
            client->status = TO_CLOSE;
        } else {
            printf("[SERVER] El cliente %d, preparado para leer\n", client->fd);
            client->status = TO_READ;
        }
    }

    client->force_status = 0;
}

void run(struct Server *server) {
    uint8_t running = 1;
    printf("[SERVER] Esperando conexiones ...\n");
    struct epoll_event events[MAX_CLIENTS];
    int ready_fds;

    while (running) {
        check(ready_fds = epoll_wait(server->epoll_fd, events, MAX_CLIENTS - 1, -1), "[SERVER] Error epoll_wait");

        printf("[SERVER] El servidor esta usando %d descriptores de archivo (max %d)\n", ready_fds, MAX_CLIENTS);
        for (size_t i = 0; i < ready_fds; i++) {
            if (events[i].data.fd == server->fd) {
                accept_new_client(server);
            } else {
                Client *client = events[i].data.ptr;
                printf("[SERVER] El cliente %d está haciendo algo\n", client->fd);

                if ((events[i].events & EPOLLRDHUP) || (events[i].events & EPOLLHUP)) {
                    printf("[SERVER] El cliente %d se ha desconectado\n", client->fd);
                    del_client(server, client);
                } else {

                    serve_client(server, client);

                    struct epoll_event ev;
                    ev.data.ptr = client;
                    switch (client->status) {
                    case TO_READ:
                        printf("[SERVER] Configurando el cliente %d para lectura\n", client->fd);
                        ev.events = EPOLLIN | EPOLLRDHUP | EPOLLHUP;
                        check(epoll_ctl(server->epoll_fd, EPOLL_CTL_MOD, client->fd, &ev), "[SERVER] Error epoll_ctl mod");
                        break;

                    case TO_WRITE:
                        printf("[SERVER] Configurando el cliente %d para escritura\n", client->fd);
                        ev.events = EPOLLOUT | EPOLLRDHUP | EPOLLHUP;
                        check(epoll_ctl(server->epoll_fd, EPOLL_CTL_MOD, client->fd, &ev), "[SERVER] Error epoll_ctl mod");
                        break;

                    case TO_CLOSE:
                        printf("[SERVER] El cliente %d se a desconectado\n", client->fd);
                        del_client(server, client);
                        break;

                    default:
                        printf("[SERVER] No se puede determinar el estado del cliente %d\n", client->fd);
                        break;
                    }
                }
            }
        }
    }
}

int stop_server(struct Server *server) {
    check(shutdown(server->fd, SHUT_RDWR), "[SERVER] Error shutdown");

    free(server->clients);
    free(server);

    return 0;
}