#include <client.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv) {

    if (argc < 3) {
        fprintf(stderr, "Uso: client <address> <port>\n");
        return 1;
    }

    struct Server *server = connect_server(argv[1], atoi(argv[2]));

    run(server);

    close_server(server);

    return 0;
}
